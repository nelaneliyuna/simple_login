var mysql = require('mysql');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');
var env = require('dotenv').config()
var md5 = require('md5');

var connection = mysql.createConnection({
	host     : process.env.DB_HOST,
	user     : process.env.DB_USER,
	password : process.env.DB_PASS,
	database : process.env.DB_NAME
});

connection.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    let sql = `INSERT INTO accounts (username, password, email) VALUES ?`;
    let values = [
        ['test', 'test#', 'test@gmail.com'],
        ['user', 'password', 'user@gmail.com']
    ];
    connection.query(sql,[values], function (err, result) {
      if (err) throw err;
      console.log("Number of records inserted: " + result.affectedRows);
    });
  });