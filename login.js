var mysql = require('mysql');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');
var env = require('dotenv').config()
var md5 = require('md5');

var connection = mysql.createConnection({
	host     : process.env.DB_HOST,
	user     : process.env.DB_USER,
	password : process.env.DB_PASS,
	database : process.env.DB_NAME
});

var app = express();
app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.get('/', function(request, response) {
	response.sendFile(path.join(__dirname + '/login.html'));
});

app.post('/create_user', function(request, response) {
    let username = request.body.username;
	let password = request.body.password;
	let email = request.body.email;
	if(username == password){
		response.send('Choose different username and password');
	}
	else{
		if(request.session.loggedin){
			if (username && password && email) {
				password = md5(password);
				connection.query(`INSERT INTO accounts (username, password, email) VALUES ('${username}', '${password}', '${email}')`, function(error, results) {
					// console.log('results', results)
					if(error){
						response.send(`Error create account ${username}`);
					}
					else{
						response.send(`Success create account ${username}`);
					}
				});
			} else {
				response.send('Please enter Username, Email, and Password!');
				// response.end();
			}
		}
		else {
			response.send('Please login to create user!');
		}
	}

	// response.end();
})

app.post('/update_password', function(request, response) {
    let username = request.body.username;
	let old_password = request.body.old_password;
	let new_password = request.body.new_password;

	if(old_password == new_password){
		response.send('Please enter different new password');
	}
	else{
		if(request.session.loggedin){
			if (username && new_password) {
				new_password = md5(new_password);
				connection.query(`UPDATE accounts SET password = '${new_password}' WHERE username = '${username}'`, function(error, results) {
					// console.log('results', results)
					if(error){
						response.send(`Error change password account ${username}`);
					}
					else{
						response.send(`Success change password account ${username}`);
					}
				});
			} else {
				response.send('Please enter Username and Password!');
			}
		}
	}

})

app.post('/auth', function(request, response) {
	let username = request.body.username;
	let password = request.body.password;
	if (username && password) {
        password = md5(password);
        connection.query('SELECT * FROM accounts WHERE username = ? AND password = ?', [username, password], function(error, results, fields) {
            // console.log('results', results)
            if(error){
                console.log('error', error)
            }
            if (results.length > 0) {
				request.session.loggedin = true;
				request.session.username = username;
				response.redirect('/home');
			} else {
				response.send('Incorrect Username and/or Password!');
			}
			// response.end();
		});
	} else {
		response.send('Please enter Username and Password!');
		// response.end();
	}
});

app.get('/home', function(request, response) {
	if (request.session.loggedin) {
		response.send('Succes login');
	} else {
		response.send('Please login to view this page!');
	}
	// response.end();
});

app.listen(3000);