# simple_login

A. Local Setup:

run this instruction in directory simple_login:
npm init
npm install
node seeder_create_db.js
node seeder_create_table.js
node seeder_default_user.js
node login.js

B. Using Postman:

1. Login
POST: localhost:3000/auth
body: {
    "username":"test",
    "password":"test#"
}

for the default user use: username: test, password: test# or username: user, password: password.
but next, you can create new user and use it for new login.

2. Create User
POST: localhost:3000/create_user
body: {
    "username":"test111",
    "password":"test111",
    "email":"hehe@gmail.com"
}

3. Update password
POST: localhost:3000/update_password
body: {
    "username":"test",
    "old_password":"test",
    "new_password":"testlagi"
}
